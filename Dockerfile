FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine as build
WORKDIR /app
COPY *.sln .
COPY Base64Api/*.csproj ./Base64Api/
COPY ClassLibrary1/*.csproj ./ClassLibrary1/
COPY TestProject1/*.csproj ./TestProject1/
RUN dotnet restore

COPY Base64Api/. ./Base64Api/
COPY ClassLibrary1/. ./ClassLibrary1/
WORKDIR /app/Base64Api
RUN dotnet publish -c Release -o release

FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine as runtime
ENV ASPNETCORE_URLS http://*:5000
WORKDIR /app
COPY --from=build /app/Base64Api/release ./
ENTRYPOINT ["dotnet", "Base64Api.dll"]

