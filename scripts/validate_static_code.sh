
echo "Check static code"

REPORT_PATH="reports/report"

FILTER="Base64Encryptor.cs"

OBS=$(cat $REPORT_PATH | grep $FILTER)

echo $OBS

if [ -z "$OBS" ]
then
    echo "All OK!"
    exit 0
fi

cat $REPORT_PATH

exit 1