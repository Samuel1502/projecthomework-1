#! /bin/bash
WARNINGS_PATH="./buildWarnings.txt"

MAX_WARNINGS_THRESHOLD="$1"

ACTUAL_WARNINGS_COUNT=$(cat $WARNINGS_PATH | sed '/^\s*#/d;/^\s*$/d' | wc -l)

if [ "$ACTUAL_WARNINGS_COUNT" -gt "$MAX_WARNINGS_THRESHOLD" ];
then
    echo "Error, Warnings count ${ACTUAL_WARNINGS_COUNT} should be less than ${MAX_WARNINGS_THRESHOLD}"
    exit 1
fi

echo "Ok! Warnings count: ${ACTUAL_WARNINGS_COUNT}"
exit 0