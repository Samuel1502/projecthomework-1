#! /bin/bash

echo "Check Coverage"

COVERAGE_REPORT_PATH="./reports/coverage.cobertura.xml"
MIN_COVERAGE_RATE="$1"
echo "$MIN_COVERAGE_RATE"
RATE_REGEX='[0-1]\d*(\.\d+)?'


COVERAGE_RATE=$(awk 'NR==2' $COVERAGE_REPORT_PATH | grep -Eio "line-rate=\"(?[0-9]+\.?[0-9]*)\"" | grep -Eo '["\047].*["\047]' | tr -d '"')


if [ "$COVERAGE_RATE" == 1 ];
then
    echo "Pass Successful"
    exit 0
fi


COVERAGE_RATE=$(awk 'NR==2' $COVERAGE_REPORT_PATH | grep -Eio "line-rate=\"(?[0-9]+\.?[0-9]*)\"" | grep -Eo '["\047].*["\047]' | tr -d '"'| cut -d "." -f2 |  cut -c1-2)
LENGHT=${#COVERAGE_RATE}
if [ "$LENGHT" == "1" ];
then
    COVERAGE_RATE="${COVERAGE_RATE}0"
fi


if [ "$COVERAGE_RATE" -ge "$MIN_COVERAGE_RATE" ];
then
    echo "Pass Successful"
    exit 0
fi

echo "$COVERAGE_RATE should be at least $MIN_COVERAGE_RATE"
exit 1