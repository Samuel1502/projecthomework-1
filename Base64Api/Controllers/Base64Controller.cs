﻿namespace Base64Api.Controllers
{
	using Microsoft.AspNetCore.Mvc;
	using ClassLibrary1;
	using Base64Api.Interfaces;

	[ApiController]
	[Route("api/v{version:apiVersion}/encryption/base64")]
	[ApiVersion("1.0")]
	public class Base64Controller : ControllerBase
	{
		private readonly ILoggerManager _logger;

		public Base64Controller(ILoggerManager logger)
		{
			_logger = logger;
		}

		[HttpGet("encrypt/{stringToEncrypt}")]
		public IActionResult EncriptText(string stringToEncrypt)
		{
			string encryptedText = Base64Encryptor.EncryptBase64(stringToEncrypt);
			_logger.LogInformation($"Encrypted text: {encryptedText}");
			return Ok(encryptedText);
		}

		[HttpGet("decrypt/{base64ToDecode}")]
		public IActionResult DecryptText(string base64ToDecode)
		{
			string decryptedText;
			try
			{
				decryptedText = Base64Encryptor.DecryptBase64(base64ToDecode);
				
			}
			catch (FormatException ex)
			{
				_logger.LogError($"Invalid base64 code",ex);
				return BadRequest("Invalid base64 code");
			}

			_logger.LogInformation($"Decrypted text: {decryptedText}");
			return Ok(decryptedText);



		}

	}
}
